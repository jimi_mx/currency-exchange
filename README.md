# README #

1 Assumptions, ignored requirements, faced issues and constructive feedback.

1.1. To follow the installation instructions on CentOS 6 or newer, it assumes at least some basic linux terminal knowledge, how to connect to SSH in case the needed linux box is accessed only remotely, and if it is, then also knowledge of Git to check out the source repository into the remote linux box.

1.2. It needs at least CentOS 6 or 7 minimal installation with internet access already configured and enabled and root access, either through SSH or locally.


2 CentOS Installation.


2.1. Login as root on a bash terminal and execute the following steps:

2.1.1. Clone the Git repository in https://bitbucket.org/raulgd/currency-exchange.git to have it available locally.

2.2. Install MongoDB

2.2.1. First, add and enable the official MongoDB stable yum repository by running the following command:

```
#!bash

yum-config-manager --add-repo https://repo.mongodb.org/yum/redhat/mongodb-org.repo
yum-config-manager --enable mongodb-org

```

2.2.2. Install MongoDB by running the following command:

```
#!bash

yum install -y mongodb-org
```


2.2.3. Restart the mongod service by running the command:

```
#!bash

service mongod restart
```



2.3. Install Java

2.3.1. Execute the following commands:

```
#!bash

mkdir /usr/java
cd /usr/java
wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u74-b02/jdk-8u74-linux-x64.tar.gz"

```

2.3.2. Descompress by executing the following command:

```
#!bash

tar -zxvf jdk-8u74-linux-x64.tar.gz
```


2.3.3. Create a dynamic link from the folder and name it /usr/java/latest

```
#!bash

ln -s /usr/java/jdk1.8.0_74 /usr/java/latest
ln -s /usr/java/jdk1.8.0_74 /usr/java/default

```

2.3.4. Copy the alljava.sh file in the directory currency-exchange to /etc/profile.d/


2.4. Open Firewall port for public access

2.4.1. On CentoOS 6 if there is iptables installed as a service, execute the following commands to open port 8080:

```
#!bash

iptables -I INPUT -p tcp --dport 8080 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -I OUTPUT -p tcp --sport 8080 -m state --state ESTABLISHED -j ACCEPT
service iptables save

```

2.4.2. On CentOS 7, the firewall installation option now comes with firewalld as a service, if installed, execute the following commands to open port 8080:

```
#!bash

firewall-cmd --zone=public --add-port=8080/tcp --permanent
firewall-cmd --reload
```
 

2.5. Install Maven

2.5.1. The projects use Maven as a build tool, to install maven, execute the following commands:

```
#!bash

wget http://mirror.cc.columbia.edu/pub/software/apache/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.tar.gz
tar -xzf apache-maven-3.3.9-bin.tar.gz -C /usr/java
ln -s /usr/java/apache-maven-3.3.9 /usr/java/maven

```


2.6. Build and run the server application

2.6.1. Go to the checked out Git repository directory of currency-exchange and execute the following command:

```
#!bash

mvn clean install
```


2.6.2. After the build is done, a new directory called target was created under currency-exchange, go inside the target directory and execute the following command:

```
#!bash

java -jar currency-exchange-1.0.0.jar
```


2.7. You can check the application's website by opening the URL in a web browser like google chrome, if done locally, use http://localhost:8080 if remotely, just replace localhost with the server IP address or domain name.