package com.crossover.sa.rest.resources;

import com.crossover.sa.CurrencyExchangeApplication;
import com.crossover.sa.entities.Conversion;
import com.crossover.sa.entities.Rate;
import com.crossover.sa.properties.MongoProperties;
import com.mongodb.client.MongoCollection;
import org.junit.Assert;
import org.bson.Document;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Test the Currency API
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CurrencyExchangeApplication.class)
@WebAppConfiguration
public class CurrencyResourceTest {

    @Autowired
    MongoProperties mongo;

    @Autowired
    CurrencyResource curr;

    /**
     * Setup the mock rates data
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
        mongo.getDatabase().drop();

        MongoCollection<Document> col = mongo.getDatabase().getCollection("rates");

        Document d = new Document("_id", "USD")
                .append("name", "United States Dollar")
                .append("updated", new Date())
                .append("rate", "1.0");

        col.insertOne(d);

        d = new Document("_id", "MXN")
                .append("name", "Mexican Peso")
                .append("updated", new Date())
                .append("rate", "17.90806");

        col.insertOne(d);
    }

    /**
     * Destroy the mock rates data
     * @throws Exception
     */
    @After
    public void tearDown() throws Exception {
        mongo.getDatabase().drop();
    }

    /**
     * Test the list rates operation
     *
     * @throws Exception
     */
    @Test
    public void testList() throws Exception {
        List<Rate> rates = curr.list("USD");
        Assert.assertEquals(rates.size(), 2);
    }

    /**
     * Test the search rates operation
     *
     * @throws Exception
     */
    @Test
    public void testSearch() throws Exception {
        List<Rate> rates = curr.search("USD", "US");
        Assert.assertEquals(rates.size(), 1);
    }

    /**
     * Test the get rate operation
     *
     * @throws Exception
     */
    @Test
    public void testGet() throws Exception {
        Rate rate = curr.get("USD", "USD");
        Assert.assertEquals(rate.getName(), "United States Dollar");
    }

    /**
     * Test the conversion amount operation
     *
     * @throws Exception
     */
    @Test
    public void testConvert() throws Exception {
        Conversion c = curr.convert("USD", "MXN", new BigDecimal("1000.00"));
        Assert.assertEquals(c.getResult().toString(), "17908.0600000");
    }
}