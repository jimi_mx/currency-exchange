package com.crossover.sa;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CurrencyExchangeApplication.class)
@WebAppConfiguration
public class CurrencyExchangeApplicationTests {

	/**
	 * Test that the Spring Boot context is setup correctly based on all configuration parameters
	 */
	@Test
	public void contextLoads() {
	}

}
