/**
 *
 * This defines the methods needed for the interactions on button clicks, opening and closing modals, header clicks
 * and other index.html page interactions
 *
 */

/*************************************************
 * On page load
 *************************************************/
$(document).ready(function () {
    // This command is used to initialize some elements and make them work properly
    $.material.init();

    //alert toggles
    $('#cx-alert-close').click(function () {
        $('#cx-alert').addClass('collapse');
    });
    $('#conversion-close').click(function () {
        $('#conversion-alert').addClass('collapse');
    });

    //call the list service and load the list
    rateController.list($("#baseField").val(), function () {
        //on success
        rateList = '<div class="list-group" id="currency-list">';
        for (var i in rateController.rates) {
            r = rateController.rates[i];
            rateItem =
                '<div class="list-group-item" id="rate-' + r.code + '">'
                + '<h4 class="list-group-item-heading"><a href="#" data-toggle="modal" data-target="#rateModal" id="rate-title-' + r.code + '"> ' + r.code + ' </a></h4>'
                + '<p class="list-group-item-text" id="rate-content-' + r.code + '">' + r.name + '<br/>Rate: ' + r.rate + '</p>'
                + '</div>'
                + '<div class="list-group-separator-full"></div>';
            rateList += rateItem;
        }
        rateList += "</div>";
        $("#currency-list").replaceWith(rateList);
    }, function (xhr, status, errorThrown) {
        if (xhr.responseJSON.error == undefined) {
            $('#cx-alert-text').text('There was an error loading the currency list');
        }
        else {
            $('#cx-alert-text').text(xhr.responseJSON.error);
        }
        $('#cx-alert').removeClass('collapse');
    });
});

/*************************************************
 * On Search
 *************************************************/
$("#baseField").keydown(function (e) {
    if (e.which == 13) {
        e.preventDefault();
        return false;
    }
});

$("#searchField").keydown(function (e) {
    if (e.which == 13) {
        e.preventDefault();

        //call the search service and load the rates list
        rateController.search($("#baseField").val(), $("#searchField").val(), function () {
            //on success
            rateList = '<div class="list-group" id="currency-list">';
            for (var i in rateController.rates) {
                r = rateController.rates[i];
                rateItem =
                    '<div class="list-group-item" id="rate-' + r.code + '">'
                    + '<h4 class="list-group-item-heading"><a href="#" data-toggle="modal" data-target="#rateModal" id="rate-title-' + r.code + '" > ' + r.code + ' </a></h4>'
                    + '<p class="list-group-item-text" id="rate-content-' + r.code + '">' + r.name + '<br/>Rate: ' + r.rate + '</p>'
                    + '</div>'
                    + '<div class="list-group-separator-full"></div>';
                rateList += rateItem;
            }
            rateList += "</div>";
            $("#currency-list").replaceWith(rateList);
        }, function (xhr, status, errorThrown) {
            if (xhr.responseJSON.error == undefined) {
                $('#cx-alert-text').text('There was an error loading the currency list');
            }
            else {
                $('#cx-alert-text').text(xhr.responseJSON.error);
            }
            $('#cx-alert').removeClass('collapse');
        });

        return false;
    }
});

/*************************************************
 * On conversion Button click
 *************************************************/
$('#conversionButton').click(function () {
    $('#conversionModal').modal('show');
});

/*************************************************
 * On conversion form convert/close buttons click
 *************************************************/
$("#convertButton").click(function () {
    rateController.convert($('#fromField').val(), $('#toField').val(), $('#amountField').val(),
        function (obj) {
            $('#resultField').val(obj.result);
        },
        function (xhr, status, errorThrown) {
            if (xhr.responseJSON.error == undefined) {
                $('#cx-alert-text').text('There was an error doing the conversion');
            }
            else {
                $('#cx-alert-text').text(xhr.responseJSON.error);
            }
            $('#cx-alert').removeClass('collapse');
        });
    return false;
});

$("#conversionClose").click(function () {
    $('#conversionModal').modal('hide');
});
