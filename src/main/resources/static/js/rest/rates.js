/**
 *
 * This defines all REST calls to the currency API
 *
 */

/******************************************
 * AJAX calls
 ******************************************/

var rateController =
				{
					"rates": [],
					"selectedRate": null,
					"list": function(base, successCallback, errorCallback) {
						$.ajax({
							"url": "/rest/currencies/" + base,
							"type": "GET",
							"dataType": "json",
							"success": function(obj) {
								rateController.rates = obj;
								successCallback();
							},
							"error": function(xhr, status, errorThrown) {
								rateController.rates = [];
								errorCallback(xhr, status, errorThrown);
							}
						});
					},
					"search": function(base, searchParam, successCallback, errorCallback) {
						$.ajax({
							"url": "/rest/currencies/" + base + "/search?q=" + searchParam,
							"type": "GET",
							"dataType": "json",
							"success": function(obj) {
								rateController.rates = obj;
								successCallback();
							},
							"error": function(xhr, status, errorThrown) {
								rateController.rates = [];
								errorCallback(xhr, status, errorThrown);
							}
						});
					},
					"get": function(base, currency, successCallback, errorCallback) {
						$.ajax({
							"url": "/rest/currencies/" + base + "/" + currency,
							"type": "GET",
							"dataType": "json",
							"success": function(obj) {
								successCallback(obj);
							},
							"error": function (xhr, status, errorThrown) {
								errorCallback(xhr, status, errorThrown);
							}
						});
					},
					"convert": function (base, currency, amount, successCallback, errorCallback) {
						$.ajax({
							"url": "/rest/currencies/" + base + "/" + currency + "/" + amount,
							"type": "GET",
							"dataType": "json",
							"success": function(obj) {
								successCallback(obj);
							},
							"error": function (xhr, status, errorThrown) {
								errorCallback(xhr, status, errorThrown);
							}
						});
					}
				};