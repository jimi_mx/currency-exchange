package com.crossover.sa.properties;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Load all the configuration properties for the MongoDB database connection setup and create the Mongo client
 * connection pool
 */
@ConfigurationProperties(prefix = "mongodb")
public class MongoProperties {

    private String host = "localhost";
    private int port = 27017;
    private String dbName = "currency-exchange";

    private static MongoClient mongoClient;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public MongoDatabase getDatabase() {
        if (mongoClient == null) {
            mongoClient = new MongoClient(host, port);
        }
        return mongoClient.getDatabase(dbName);
    }
}
