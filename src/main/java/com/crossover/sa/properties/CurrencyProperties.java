package com.crossover.sa.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Loads all the configuration properties for the openexchangerates.org API and MongoDB TTL collection
 */
@ConfigurationProperties(prefix = "currency")
public class CurrencyProperties {

    private String appId;
    private long cacheExpire = 3600;
    private boolean allowExperimentalRates = false;

    private static String apiUrl = "http://openexchangerates.org/api";

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public long getCacheExpire() {
        return cacheExpire;
    }

    public void setCacheExpire(long cacheExpire) {
        this.cacheExpire = cacheExpire;
    }

    public boolean isAllowExperimentalRates() {
        return allowExperimentalRates;
    }

    public int showExperimental() {
        if (allowExperimentalRates) {
            return 1;
        } else {
            return 0;
        }
    }

    public void setAllowExperimentalRates(boolean allowExperimentalRates) {
        this.allowExperimentalRates = allowExperimentalRates;
    }

    public String getApiUrl() {
        return apiUrl;
    }
}
