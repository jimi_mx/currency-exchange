package com.crossover.sa;

import com.crossover.sa.properties.CurrencyProperties;
import com.crossover.sa.properties.MongoProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Setup the configuration properties classes from the application.properties file
 */
@Configuration
@EnableScheduling
@EnableConfigurationProperties({CurrencyProperties.class, MongoProperties.class})
public class CurrencyExchangeAutoConfiguration {
}
