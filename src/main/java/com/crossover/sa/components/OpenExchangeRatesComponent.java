package com.crossover.sa.components;

import com.crossover.sa.entities.CurrencyRate;
import com.crossover.sa.properties.CurrencyProperties;
import com.crossover.sa.properties.MongoProperties;
import com.crossover.sa.rest.client.RestClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.mongodb.client.model.Filters.eq;

/**
 * This class loads and refreshes the MongoDB database from the openexchangerates.org REST API
 */
@Component
public class OpenExchangeRatesComponent {

    @Autowired
    MongoProperties mongo;

    @Autowired
    CurrencyProperties currency;

    /**
     * Loads the available currencies from the API and creates or refreshes the MongoDB currencies collection, it also
     * runs as a scheduled job that refreshes the currencies list from the API every day at midnight
     */
    @Scheduled(cron = "0 0 0 * * ?")
    public void reloadCurrencies() {
        Map<String, String> map = null;

        WebTarget target = RestClient.getClient().target(currency.getApiUrl()
                + "/currencies.json?app_id=" + currency.getAppId()
                + "&show_experimental=" + String.valueOf(currency.showExperimental()));
        map = target.request(MediaType.APPLICATION_JSON).get(new GenericType<Map<String, String>>(){});

        if (!map.isEmpty()) {
            MongoCollection<Document> col = mongo.getDatabase().getCollection("currencies");
            MongoCursor<Document> cursor = col.find().iterator();
            try {
                while (cursor.hasNext()) {
                    Document d = cursor.next();
                    String currencyCode = d.getString("_id");
                    if (!map.containsKey(currencyCode)) {
                        col.deleteOne(d);
                    } else {
                        map.remove(currencyCode);
                    }
                }
                List<Document> documents = new ArrayList<>();
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    Document d = new Document("_id", entry.getKey()).append("name", entry.getValue());
                    documents.add(d);
                }
                if (!documents.isEmpty()) {
                    col.insertMany(documents);
                }
            } finally {
                cursor.close();
            }
        }

        getCurrency();
    }

    /**
     * Get the latest currency data from the API using USD as base, this is done because the free version of the API
     * is limited to only use USD as a base currency.
     *
     * This application will make the necessary conversion operations to support different base rates to work around
     * this limitation.
     */
    public void getCurrency() {
        WebTarget target = RestClient.getClient().target(currency.getApiUrl()
                + "/latest.json?app_id=" + currency.getAppId() + "&base=USD"
                + "&show_experimental=" + String.valueOf(currency.showExperimental()));
        CurrencyRate c = target.request(MediaType.APPLICATION_JSON).get(CurrencyRate.class);

        MongoCollection<Document> col = mongo.getDatabase().getCollection("rates");
        MongoCollection<Document> currenciesCol = mongo.getDatabase().getCollection("currencies");

        if (col.count() == 0) {
            List<Document> documents = new ArrayList<>();
            for (Map.Entry<String, BigDecimal> entry : c.getRates().entrySet()) {
                Document currenciesDocument = currenciesCol.find(eq("_id", entry.getKey())).first();
                Document d = new Document("_id", entry.getKey())
                        .append("name", currenciesDocument.getString("name"))
                        .append("updated", new Date())
                        .append("rate", entry.getValue().toString());
                documents.add(d);
            }
            if (!documents.isEmpty()) {
                col.insertMany(documents);
            }
        }
    }

    /**
     * Checks the MongoDB database if the rates collection TTL has expired, if it has, then get the latest currency
     * data from the API
     */
    public void validateCache() {
        MongoCollection<Document> col = mongo.getDatabase().getCollection("rates");
        if (col.count() == 0) {
            getCurrency();
        }
    }
}
