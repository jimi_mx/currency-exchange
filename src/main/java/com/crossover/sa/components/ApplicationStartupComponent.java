package com.crossover.sa.components;

import com.crossover.sa.properties.CurrencyProperties;
import com.crossover.sa.properties.MongoProperties;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.IndexOptions;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

;import java.util.concurrent.TimeUnit;

/**
 * Executes the onApplicationEvent method right after the Spring Boot application has booted
 */
@Component
public class ApplicationStartupComponent implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    MongoProperties mongo;

    @Autowired
    CurrencyProperties currency;

    @Autowired
    OpenExchangeRatesComponent oxr;

    /**
     * Setup the MongoDB currency-exchange database
     *
     * @param contextRefreshedEvent
     */
    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        MongoDatabase db = mongo.getDatabase();

        //check if the needed collections exist and setted up correctly, if not, create them
        boolean currenciesExists = false;
        boolean ratesExists = false;

        for (String name : db.listCollectionNames()) {
            switch (name) {
                case "currencies":
                    currenciesExists = true;
                    break;
                case "rates":
                    ratesExists = true;
                    break;
            }
        }

        if (!currenciesExists) {
            db.createCollection("currencies");
        }
        oxr.reloadCurrencies();

        if (!ratesExists) {
            MongoCollection<Document> col = db.getCollection("rates");
            col.createIndex(new Document("updated", 1), new IndexOptions()
                    .expireAfter(currency.getCacheExpire(), TimeUnit.MINUTES));
        }
    }
}
