package com.crossover.sa.components;

import com.crossover.sa.entities.Conversion;
import com.crossover.sa.entities.Rate;
import com.crossover.sa.properties.MongoProperties;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import static com.mongodb.client.model.Filters.or;
import static com.mongodb.client.model.Filters.regex;

/**
 * Contains the currency business logic
 */
@Component
public class CurrencyComponent {

    @Autowired
    MongoProperties mongo;

    @Autowired
    OpenExchangeRatesComponent oxc;

    /**
     * Lists all the supported currencies using a base currency for the conversion rate value
     *
     * @param baseCurrency the base currency in the 3-letter code format
     * @return a list of Rate objects
     */
    public List<Rate> list(String baseCurrency) {
        List<Rate> rates = new ArrayList<>();
        oxc.validateCache();

        MongoCollection<Document> col = mongo.getDatabase().getCollection("rates");

        Document baseDocument = col.find(new Document("_id", baseCurrency)).first();
        Rate base = new Rate();
        base.setCode(baseDocument.getString("_id"));
        base.setRate(new BigDecimal(baseDocument.getString("rate")));

        MongoCursor<Document> cursor = col.find().sort(new Document("_id", 1)).iterator();
        try {
            while (cursor.hasNext()) {
                rates.add(setRateDocument(base, cursor.next()));
            }
        } finally {
            cursor.close();
        }

        return rates;
    }

    /**
     * Lists any matching currencies setted by the search parameter, using the currency code and name as filters
     *
     * @param baseCurrency the base currency in the 3-letter code format
     * @param searchParam the search parameter filter applied to the code and name fields
     * @return a list of Rate objects
     */
    public List<Rate> search(String baseCurrency, String searchParam) {
        List<Rate> rates = new ArrayList<>();
        oxc.validateCache();

        MongoCollection<Document> col = mongo.getDatabase().getCollection("rates");

        Document baseDocument = col.find(new Document("_id", baseCurrency)).first();
        Rate base = new Rate();
        base.setCode(baseDocument.getString("_id"));
        base.setRate(new BigDecimal(baseDocument.getString("rate")));

        Pattern exp = Pattern.compile(searchParam, Pattern.CASE_INSENSITIVE);
        MongoCursor<Document> cursor = col
                .find(or(regex("_id", exp), regex("name", exp)))
                .sort(new Document("_id", 1))
                .iterator();
        try {
            while (cursor.hasNext()) {
                rates.add(setRateDocument(base, cursor.next()));
            }
        } finally {
            cursor.close();
        }

        return rates;
    }

    /**
     * Get a currency by using its 3-letter code as key
     *
     * @param baseCurrency the base currency in the 3-letter code format
     * @param currency the wanted currency in the 3-letter code format
     * @return a Rate object
     */
    public Rate get(String baseCurrency, String currency) {
        Rate r = new Rate();
        oxc.validateCache();

        MongoCollection<Document> col = mongo.getDatabase().getCollection("rates");

        Document baseDocument = col.find(new Document("_id", baseCurrency)).first();
        Rate base = new Rate();
        base.setCode(baseDocument.getString("_id"));
        base.setRate(new BigDecimal(baseDocument.getString("rate")));

        return setRateDocument(base, col.find(new Document("_id", currency)).first());
    }

    /**
     * Converts a currency amount from one currency type to another
     *
     * @param from the base currency in the 3-letter code format
     * @param to the converted currency in the 3-letter code format
     * @param amount the amount of currency to be converted
     * @return a Conversion object with the resulting amount
     */
    public Conversion convert(String from, String to, BigDecimal amount) {
        Conversion c = new Conversion();
        oxc.validateCache();

        Rate r = get(from, to);
        c.setFrom(from);
        c.setTo(to);
        c.setAmount(amount);
        c.setResult(amount.multiply(r.getRate()));

        return c;
    }

    /**
     * Convert a currency from one rate to another
     *
     * @param baseRate the base currency rate amount in BigDecimal representation
     * @param rate the converted currency rate amount in BigDecimal representation
     * @return the resulting converted rate amount in BigDecimal representation
     */
    private BigDecimal convertBaseRate(BigDecimal baseRate, BigDecimal rate) {
        return rate.multiply(BigDecimal.ONE.divide(baseRate, new MathContext(6, RoundingMode.HALF_EVEN)));
    }

    /**
     * Create a Rate object from a MongoDB document with a converted currency rated from a base Rate object
     *
     * @param base a base currency Rate object
     * @param d a MongoDB document with an internal Rate structure
     * @return a Rate object with the document data and base rate conversion
     */
    private Rate setRateDocument(Rate base, Document d) {
        Rate r = new Rate();
        r.setBase(base.getCode());
        r.setCode(d.getString("_id"));
        r.setName(d.getString("name"));
        r.setUpdated(d.getDate("updated"));
        BigDecimal baseRate = base.getRate();
        BigDecimal rate = new BigDecimal(d.getString("rate"));
        r.setRate(convertBaseRate(baseRate, rate));

        return r;
    }
}
