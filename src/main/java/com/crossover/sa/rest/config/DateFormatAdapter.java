package com.crossover.sa.rest.config;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Set a date java format adapter codec for java.util.Date objects and standard ISO date strings
 */
public class DateFormatAdapter extends XmlAdapter<String, Date> {

    private String datePattern = "yyyy-MM-dd'T'HH:mm:ss'-00:00'";

    /**
     * Converts a String formatted as an ISO Date to a Java Date object
     *
     * @param d the date string
     * @return a Date object
     * @throws Exception if the date String is not formed correctly
     */
    @Override
    public Date unmarshal(String d) throws Exception {
        return new SimpleDateFormat(datePattern).parse(d);
    }

    /**
     * Converts a Java Date object into an ISO formatted date String
     *
     * @param d the Date object
     * @return a Date String
     * @throws Exception Exception if the Date object is null or not valid
     */
    @Override
    public String marshal(Date d) throws Exception {
        return new SimpleDateFormat(datePattern).format(d);
    }
}