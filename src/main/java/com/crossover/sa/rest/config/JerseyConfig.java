package com.crossover.sa.rest.config;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import javax.ws.rs.ApplicationPath;

/**
 * Configure the Jersey REST JAX-RS library
 */
@Configuration
@ApplicationPath("/rest")
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        //configure to use Jackson as default JSON serialization provider
        register(JacksonFeature.class);

        //scan for providers and resource components
        packages(true, "com.crossover.sa.rest");
    }
}
