package com.crossover.sa.rest.resources;

import com.crossover.sa.components.CurrencyComponent;
import com.crossover.sa.entities.Conversion;
import com.crossover.sa.entities.Rate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;
import java.util.List;

/**
 * Spring component/Jersey Resource class to define the currency exchange REST API endpoints
 */
@Path("/currencies")
@Component
public class CurrencyResource {

    @Autowired
    CurrencyComponent currency;

    /**
     * Lists all the supported currencies using a base currency for the conversion rate value
     *
     * @param baseCurrency the base currency in the 3-letter code format
     * @return a list of Rate objects
     */
    @GET
    @Path("/{base}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Rate> list(@PathParam("base") String baseCurrency) {
        return currency.list(baseCurrency);
    }

    /**
     * Lists any matching currencies setted by the search parameter, using the currency code and name as filters
     *
     * @param baseCurrency the base currency in the 3-letter code format
     * @param searchParam the search parameter filter applied to the code and name fields
     * @return a list of Rate objects
     */
    @GET()
    @Path("/{base}/search")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Rate> search(@PathParam("base") String baseCurrency, @QueryParam("q") String searchParam) {
        return currency.search(baseCurrency, searchParam);
    }

    /**
     * Get a currency by using its 3-letter code as key
     *
     * @param baseCurrency baseCurrency the base currency in the 3-letter code format
     * @param cur the wanted currency in the 3-letter code format
     * @return a Rate object
     */
    @GET
    @Path("/{base}/{currency}")
    @Produces(MediaType.APPLICATION_JSON)
    public Rate get(@PathParam("base") String baseCurrency, @PathParam("currency") String cur) {
        return currency.get(baseCurrency, cur);
    }

    /**
     * Converts a currency amount from one currency type to another
     *
     * @param from the base currency in the 3-letter code format
     * @param to the converted currency in the 3-letter code format
     * @param amount the amount of currency to be converted
     * @return a Conversion object with the resulting amount
     */
    @GET
    @Path("/{from}/{to}/{amount}")
    @Produces(MediaType.APPLICATION_JSON)
    public Conversion convert(@PathParam("from") String from, @PathParam("to") String to,
                              @PathParam("amount") BigDecimal amount) {
        return currency.convert(from, to, amount);
    }
}
