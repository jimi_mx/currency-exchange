package com.crossover.sa.rest.client;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class HTTPSimpleAuthenticator implements ClientRequestFilter {

    private final String user;
    private final String password;

    public HTTPSimpleAuthenticator(String user, String password) {
        this.user = user;
        this.password = password;
    }

    @Override
    public void filter(ClientRequestContext requestContext) throws IOException {
        MultivaluedMap<String, Object> headers = requestContext.getHeaders();
        String basicAuthentication = getBasicAuthentication();
    }

    private String getBasicAuthentication() {
        String token = this.user + ":" + this.password;
        try {
            return DatatypeConverter.printBase64Binary(token.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException("cannot encode Authentication header with UTF-8", e);
        }
    }
}
