package com.crossover.sa.rest.client;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

public class RestClient {
    public static Client getClient() {
        return ClientBuilder.newClient();
    }
}
